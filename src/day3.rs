use std::collections::HashSet;
use std::fs;

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
pub struct Point {
    x: isize,
    y: isize,
}

impl Point {
    pub fn new(x: isize, y: isize) -> Self {
        Self { x, y }
    }

    pub fn next_point(&mut self, dir: char) {
        match dir {
            '^' => self.y += 1,
            '>' => self.x += 1,
            'v' => self.y -= 1,
            '<' => self.x -= 1,
            _ => (),
        };
    }
}

pub fn run() {
    println!("Day 3:");
    let input = fs::read_to_string("./inputs/day3.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(directions: &str) -> usize {
    let mut location = Point::new(0, 0);
    let mut prev_locs = HashSet::new();
    prev_locs.insert(location);

    for dir in directions.chars() {
        location.next_point(dir);
        prev_locs.insert(location);
    }

    prev_locs.len()
}

fn part2(directions: &str) -> usize {
    let mut santa = Point::new(0, 0);
    let mut robo = Point::new(0, 0);
    let mut prev_locs = HashSet::new();
    prev_locs.insert(santa);

    for (i, dir) in directions.chars().enumerate() {
        if i % 2 == 0 {
            santa.next_point(dir);
            prev_locs.insert(santa);
        } else {
            robo.next_point(dir);
            prev_locs.insert(robo);
        }
    }

    prev_locs.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part1(">"), 2);
        assert_eq!(part1("^>v<"), 4);
        assert_eq!(part1("^v^v^v^v^v"), 2);
    }
    #[test]
    fn test_part_2() {
        assert_eq!(part2(">>"), 2);
        assert_eq!(part2("^>v<"), 3);
        assert_eq!(part2("^v^v^v^v^v"), 11);
    }
}
