use std::fs;

use itertools::Itertools;

pub fn run() {
    println!("Day 5:");
    let input = fs::read_to_string("./inputs/day5.txt").expect("Could not read file");
    // dbg!(input.)

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(input: &str) -> usize {
    let mut nice = 0;

    for txt in input.split_whitespace() {
        let mut blacklist_count = 0;
        let blacklist = ["ab", "cd", "pq", "xy"];
        for black in blacklist {
            if txt.contains(black) {
                blacklist_count += 1;
            }
        }
        if blacklist_count != 0 {
            continue;
        }

        let mut repeat_count = 0;
        for (l, r) in txt.chars().tuple_windows() {
            if l == r {
                repeat_count += 1;
            }
        }
        if repeat_count == 0 {
            continue;
        }

        let vowels = "aeiou";
        let mut vowel_count = 0;
        for c in txt.chars() {
            if vowels.contains(c) {
                vowel_count += 1;
            }
        }

        if vowel_count < 3 {
            continue;
        }

        nice += 1;
    }

    nice
}

fn part2(input: &str) -> usize {
    let mut nice = 0;

    for txt in input.split_whitespace() {
        let mut double = false;
        for (l, r) in txt
            .chars()
            .tuple_windows::<(_, _)>()
            .enumerate()
            .tuple_combinations()
        {
            if l.1 == r.1 && l.0.abs_diff(r.0) > 1 {
                double = true;
            }
        }
        if !double {
            continue;
        }

        let mut double_split = false;
        for (l, _, r) in txt.chars().tuple_windows::<(_, _, _)>() {
            if l == r {
                double_split = true;
            }
        }
        if double_split {
            nice += 1;
        }
    }

    nice
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1_examples() {
        assert_eq!(part1("ugknbfddgicrmopn"), 1);
        assert_eq!(
            part1("ugknbfddgicrmopn\naaa\njchzalrnumimnmhp\nhaegwjzuvuyypxyu\ndvszwmarrgswjxmb"),
            2
        );
    }

    #[test]
    fn test_1_input() {
        assert_eq!(
            part1(&fs::read_to_string("./inputs/day5.txt").expect("Could not read file")),
            238
        );
    }

    #[test]
    fn test_2_examples() {
        assert_eq!(part2("qjhvhtzxzqqjkmpb"), 1);
        assert_eq!(part2("xxyxx"), 1);
        assert_eq!(part2("aaa"), 0);
        assert_eq!(part2("aaaaa"), 1);
        assert_eq!(
            part2("qjhvhtzxzqqjkmpb\nxxyxx\nuurcxstgmygtbstg\nieodomkazucvgmuy"),
            2
        );
    }
    #[test]
    fn test_2_input() {
        assert_eq!(
            part2(&fs::read_to_string("./inputs/day5.txt").expect("Could not read file")),
            69
        );
    }
}
