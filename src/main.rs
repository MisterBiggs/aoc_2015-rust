mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;

fn main() {
    println!("Running Advent of Code 2021");
    day1::run();
    day2::run();
    day3::run();
    day4::run();
    day5::run();
    day6::run();
}
