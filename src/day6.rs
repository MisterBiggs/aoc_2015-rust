use regex::Regex;
use std::collections::HashMap;
use std::fs;

pub fn run() {
    println!("Day 6:");
    let input = fs::read_to_string("./inputs/day6.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

#[derive(Debug)]
enum Operation {
    Off,
    On,
    Toggle,
}

#[derive(Eq, Hash, PartialEq, Debug)]
pub struct Point {
    x: usize,
    y: usize,
}

impl Point {
    pub fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }
}

fn command(cmd: &str) -> (Operation, (Point, Point)) {
    let op: Operation;

    if Regex::new(r"^turn on \d").unwrap().is_match(cmd) {
        op = Operation::On;
    } else if cmd.contains("turn off") {
        op = Operation::Off;
    } else if cmd.contains("toggle") {
        op = Operation::Toggle;
    } else {
        panic!()
    }

    let re = Regex::new(r"(?P<x1>\d+),(?P<y1>\d+) through (?P<x2>\d+),(?P<y2>\d+)").unwrap();
    let caps = re.captures(cmd).unwrap();

    let start = Point::new(
        caps.name("x1").unwrap().as_str().parse::<usize>().unwrap(),
        caps.name("y1").unwrap().as_str().parse::<usize>().unwrap(),
    );

    let end = Point::new(
        caps.name("x2").unwrap().as_str().parse::<usize>().unwrap(),
        caps.name("y2").unwrap().as_str().parse::<usize>().unwrap(),
    );

    (op, (start, end))
}

// fn part1(directions: &str) -> usize {
//     let mut lights = Box::new([[false; 1000]; 1000]);
//     for dir in directions.split('\n') {
//         if dir.is_empty() {
//             continue;
//         }
//         let (operation, (start, end)) = command(dir);

//         for x in start.x..=end.x {
//             for y in start.y..=end.y {
//                 lights[x][y] = match &operation {
//                     Operation::On => true,
//                     Operation::Off => false,
//                     Operation::Toggle => !lights[x][y],
//                 }
//             }
//         }
//     }

//     let mut sum = 0;

//     for row in lights.iter() {
//         for light in row {
//             sum += *light as usize;
//         }
//     }

//     sum
// }

fn part1(directions: &str) -> usize {
    let mut lights: HashMap<Point, bool> = HashMap::with_capacity(1000000);

    for dir in directions.split('\n') {
        if dir.is_empty() {
            continue;
        }
        let (operation, (start, end)) = command(dir);
        for x in start.x..=end.x {
            for y in start.y..=end.y {
                let pt = Point::new(x, y);
                // let mut brightness = *lights.get(&pt).unwrap_or(&0);
                let brightness = match &operation {
                    Operation::On => true,
                    Operation::Off => false,
                    Operation::Toggle => *lights.get(&pt).unwrap_or(&false),
                };
                lights.insert(pt, brightness);
            }
        }
    }

    let mut total_brightness = 0;

    for brightness in lights.values() {
        total_brightness += *brightness as usize;
    }

    total_brightness
}

fn part2(directions: &str) -> usize {
    let mut lights: HashMap<Point, usize> = HashMap::with_capacity(1000000);

    for dir in directions.split('\n') {
        if dir.is_empty() {
            continue;
        }
        let (operation, (start, end)) = command(dir);
        for x in start.x..=end.x {
            for y in start.y..=end.y {
                let pt = Point::new(x, y);
                let mut brightness = *lights.get(&pt).unwrap_or(&0);
                brightness = match &operation {
                    Operation::Toggle => brightness + 2,
                    Operation::On => brightness + 1,
                    Operation::Off => brightness.saturating_sub(1),
                };
                lights.insert(pt, brightness);
            }
        }
    }

    let mut total_brightness = 0;

    for brightness in lights.values() {
        total_brightness += brightness;
    }

    total_brightness
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_all_on() {
        assert_eq!(part1("turn on 0,0 through 999,999\n"), 1000 * 1000);
    }

    #[test]
    fn part1_all_on_then_middle_off() {
        assert_eq!(
            part1("turn on 0,0 through 999,999\nturn off 499,499 through 500,500"),
            1000 * 1000 - 4
        );
    }
    #[test]
    fn part1_all_commands() {
        assert_eq!(
            part1("turn on 0,0 through 999,999\ntoggle 0,0 through 999,0\nturn off 499,499 through 500,500"),
            1000 * 1000 - 4 - 1000
        );
    }
    #[test]
    fn part1_input() {
        let input = fs::read_to_string("./inputs/day6.txt").expect("Could not read file");
        assert_eq!(part1(&input), 400410);
    }
    #[test]
    fn part2_one_light() {
        assert_eq!(part2("turn on 0,0 through 0,0"), 1);
    }
    #[test]
    fn part2_all_toggle() {
        assert_eq!(part2("toggle 0,0 through 999,999"), 2000000);
    }
    #[test]
    fn part2_all_on_of_repeat() {
        assert_eq!(
            part2(&"turn on 0,0 through 9,9\nturn off 0,0 through 9,9\n".repeat(1000)),
            0
        );
    }
}
