pub fn run() {
    println!("Day 4: [PRECALCULATED]");
    // let input = "yzbqklnj".to_string();

    println!("\tPart 1: 282749");
    println!("\tPart 2: 9962624");
    // println!("\tPart 1: {}", part1(&input, 5));
    // println!("\tPart 2: {}", part1(&input, 6));
}

#[allow(dead_code)]
fn part1(input: &str, zeros: usize) -> usize {
    let mut key: usize = 0;

    let max_iter = 1e9 as usize;
    loop {
        let digest = md5::compute(format!("{}{}", input, key));

        if format!("{:x},", digest).starts_with(&"0".repeat(zeros)) {
            return key;
        }

        key += 1;

        if key == max_iter {
            return 0;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part1("abcdef", 5), 609043);
        assert_eq!(part1("pqrstuv", 5), 1048970);
    }
}
