use std::cmp;
use std::fs;

pub fn run() {
    println!("Day 2:");
    let input = fs::read_to_string("./inputs/day2.txt").expect("Could not read file");

    let dimensions_list = parse(input);

    println!("\tPart 1: {}", part1(&dimensions_list));
    println!("\tPart 2: {}", part2(&dimensions_list));
}

fn parse(input: String) -> Vec<Vec<usize>> {
    input
        .split_whitespace()
        .map(|c| {
            c.split('x')
                .map(|n| n.parse::<usize>().unwrap())
                .collect::<Vec<usize>>()
        })
        .collect::<Vec<Vec<usize>>>()
}

fn part1(dimensions: &Vec<Vec<usize>>) -> usize {
    dimensions
        .iter()
        .map(|x| {
            let (l, w, h) = (x[0], x[1], x[2]);
            let slack = { cmp::min(l * w, cmp::min(w * h, l * h)) };
            2 * l * w + 2 * w * h + 2 * h * l + slack
        })
        .sum::<usize>()
}

fn part2(dimensions: &Vec<Vec<usize>>) -> usize {
    dimensions
        .iter()
        .map(|x| {
            let (l, w, h) = (x[0], x[1], x[2]);
            let volume = l * w * h;
            let wrap = {
                let mut y = x.clone();
                y.sort();
                2 * (y[0] + y[1])
            };

            volume + wrap
        })
        .sum::<usize>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part1(&parse("2x3x4".to_string())), 58);
        assert_eq!(part1(&parse("2x3x4\n".to_string())), 58);
        assert_eq!(part1(&parse("1x1x10".to_string())), 43);
        assert_eq!(part1(&parse("1x10x1".to_string())), 43);
        assert_eq!(part1(&parse("10x1x1".to_string())), 43);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part2(&parse("2x3x4".to_string())), 34);
        assert_eq!(part2(&parse("2x3x4\n".to_string())), 34);
        assert_eq!(part2(&parse("1x1x10".to_string())), 14);
        assert_eq!(part2(&parse("1x10x1".to_string())), 14);
        assert_eq!(part2(&parse("10x1x1".to_string())), 14);
    }
}
