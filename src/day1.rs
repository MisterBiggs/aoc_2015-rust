use std::fs;

pub fn run() {
    println!("Day 1:");
    let input = fs::read_to_string("./inputs/day1.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(directions: &str) -> isize {
    directions
        .chars()
        .map(|c| match c {
            '(' => 1,
            ')' => -1,
            _ => 0,
        })
        .sum::<isize>()
}
fn part2(directions: &str) -> usize {
    let mut floor: isize = 0;

    for (num, dir) in directions.chars().enumerate() {
        floor += match dir {
            '(' => 1,
            ')' => -1,
            _ => 0,
        };

        if floor == -1 {
            return num + 1;
        }
    }

    directions.len() + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let mut input = "(())".to_string();
        assert_eq!(part1(&input), 0);

        input = "()()".to_string();
        assert_eq!(part1(&input), 0);

        input = "(((".to_string();
        assert_eq!(part1(&input), 3);

        input = "(()(()(".to_string();
        assert_eq!(part1(&input), 3);

        input = "())".to_string();
        assert_eq!(part1(&input), -1);

        input = ")())())".to_string();
        assert_eq!(part1(&input), -3);
    }

    #[test]
    fn test_2() {
        let mut input = ")".to_string();
        assert_eq!(part2(&input), 1);

        input = "()())".to_string();
        assert_eq!(part2(&input), 5);
    }
}
